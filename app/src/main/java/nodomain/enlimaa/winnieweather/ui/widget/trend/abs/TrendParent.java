package nodomain.enlimaa.winnieweather.ui.widget.trend.abs;

public interface TrendParent {

    void setDrawingBoundary(int top, int bottom);
}
