package nodomain.enlimaa.winnieweather.main.adapter.trend.daily;

import androidx.recyclerview.widget.RecyclerView;

import nodomain.enlimaa.winnieweather.basic.GeoActivity;
import nodomain.enlimaa.winnieweather.ui.widget.trend.abs.TrendParent;
import nodomain.enlimaa.winnieweather.ui.widget.trend.abs.TrendRecyclerViewAdapter;
import nodomain.enlimaa.winnieweather.utils.helpter.IntentHelper;

public abstract class AbsDailyTrendAdapter<VH extends RecyclerView.ViewHolder> extends TrendRecyclerViewAdapter<VH>  {

    private GeoActivity activity;
    private String formattedId;

    public AbsDailyTrendAdapter(GeoActivity activity, TrendParent trendParent, String formattedId) {
        super(trendParent);
        this.activity = activity;
        this.formattedId = formattedId;
    }

    protected void onItemClicked(int adapterPosition) {
        if (activity.isForeground()) {
            IntentHelper.startDailyWeatherActivity(activity, formattedId, adapterPosition);
        }
    }
}
