package nodomain.enlimaa.winnieweather.main;

import android.content.Context;

import nodomain.enlimaa.winnieweather.ui.decotarion.ListDecoration;
import nodomain.enlimaa.winnieweather.utils.manager.ThemeManager;

/**
 * Main list decoration.
 * */

public class MainListDecoration extends ListDecoration {

    public MainListDecoration(Context context) {
        super(
                context,
                ThemeManager.getInstance(context).getLineColor(context)
        );
    }
}
