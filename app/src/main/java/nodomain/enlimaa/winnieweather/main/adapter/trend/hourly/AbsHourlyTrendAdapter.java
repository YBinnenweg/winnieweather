package nodomain.enlimaa.winnieweather.main.adapter.trend.hourly;

import androidx.recyclerview.widget.RecyclerView;

import nodomain.enlimaa.winnieweather.basic.GeoActivity;
import nodomain.enlimaa.winnieweather.basic.model.weather.Weather;
import nodomain.enlimaa.winnieweather.main.dialog.HourlyWeatherDialog;
import nodomain.enlimaa.winnieweather.ui.widget.trend.abs.TrendParent;
import nodomain.enlimaa.winnieweather.ui.widget.trend.abs.TrendRecyclerViewAdapter;
import nodomain.enlimaa.winnieweather.utils.manager.ThemeManager;

public abstract class AbsHourlyTrendAdapter<VH extends RecyclerView.ViewHolder> extends TrendRecyclerViewAdapter<VH>  {

    private GeoActivity activity;
    private Weather weather;

    public AbsHourlyTrendAdapter(GeoActivity activity, TrendParent trendParent, Weather weather) {
        super(trendParent);
        this.activity = activity;
        this.weather = weather;
    }

    protected void onItemClicked(int adapterPosition) {
        if (activity.isForeground()) {
            HourlyWeatherDialog dialog = new HourlyWeatherDialog();
            dialog.setData(weather, adapterPosition, ThemeManager.getInstance(activity).getWeatherThemeColors()[0]);
            dialog.show(activity.getSupportFragmentManager(), null);
        }
    }
}
