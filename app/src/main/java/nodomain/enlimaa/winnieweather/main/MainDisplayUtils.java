package nodomain.enlimaa.winnieweather.main;

import android.content.Context;

import nodomain.enlimaa.winnieweather.utils.DisplayUtils;

public class MainDisplayUtils {

    public static boolean isMultiFragmentEnabled(Context context) {
        return DisplayUtils.isTabletDevice(context) && DisplayUtils.isLandscape(context);
    }
}
