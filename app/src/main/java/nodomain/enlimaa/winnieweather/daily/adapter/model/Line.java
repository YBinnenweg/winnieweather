package nodomain.enlimaa.winnieweather.daily.adapter.model;

import nodomain.enlimaa.winnieweather.daily.adapter.DailyWeatherAdapter;

public class Line implements DailyWeatherAdapter.ViewModel {

    public static boolean isCode(int code) {
        return code == -1;
    }

    @Override
    public int getCode() {
        return -1;
    }
}
