package nodomain.enlimaa.winnieweather.daily.adapter.model;

import nodomain.enlimaa.winnieweather.daily.adapter.DailyWeatherAdapter;

public class Margin implements DailyWeatherAdapter.ViewModel {

    public static boolean isCode(int code) {
        return code == -2;
    }

    @Override
    public int getCode() {
        return -2;
    }
}
