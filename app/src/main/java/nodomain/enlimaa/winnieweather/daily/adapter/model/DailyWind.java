package nodomain.enlimaa.winnieweather.daily.adapter.model;

import nodomain.enlimaa.winnieweather.basic.model.weather.Wind;
import nodomain.enlimaa.winnieweather.daily.adapter.DailyWeatherAdapter;

public class DailyWind implements DailyWeatherAdapter.ViewModel {

    private Wind wind;

    public DailyWind(Wind wind) {
        this.wind = wind;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public static boolean isCode(int code) {
        return code == 4;
    }

    @Override
    public int getCode() {
        return 4;
    }
}
