package nodomain.enlimaa.winnieweather.background.polling.basic;

import android.app.NotificationChannel;
import android.app.NotificationManager;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import nodomain.enlimaa.winnieweather.WinnieWeather;
import nodomain.enlimaa.winnieweather.R;
import nodomain.enlimaa.winnieweather.basic.model.location.Location;
import nodomain.enlimaa.winnieweather.basic.model.weather.Weather;
import nodomain.enlimaa.winnieweather.db.DatabaseHelper;

/**
 * Foreground update service.
 * */

public abstract class ForegroundUpdateService extends UpdateService {

    @Override
    public void onCreate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    WinnieWeather.NOTIFICATION_CHANNEL_ID_BACKGROUND,
                    WinnieWeather.getNotificationChannelName(
                            this, WinnieWeather.NOTIFICATION_CHANNEL_ID_BACKGROUND),
                    NotificationManager.IMPORTANCE_MIN
            );
            channel.setShowBadge(false);
            channel.setLightColor(ContextCompat.getColor(this, R.color.colorPrimary));
            NotificationManagerCompat.from(this).createNotificationChannel(channel);
        }

        // version O.
        startForeground(
                getForegroundNotificationId(),
                getForegroundNotification(
                        1,
                        DatabaseHelper.getInstance(this).countLocation()
                ).build()
        );

        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // version O.
        stopForeground(true);
        NotificationManagerCompat.from(this).cancel(getForegroundNotificationId());
    }

    @Override
    public void stopService(boolean updateFailed) {
        stopForeground(true);
        NotificationManagerCompat.from(this).cancel(getForegroundNotificationId());
        super.stopService(updateFailed);
    }

    public NotificationCompat.Builder getForegroundNotification(int index, int total) {
        return new NotificationCompat.Builder(this, WinnieWeather.NOTIFICATION_CHANNEL_ID_BACKGROUND)
                .setSmallIcon(R.drawable.ic_running_in_background)
                .setContentTitle(getString(R.string.winnie_weather))
                .setContentText(getString(R.string.feedback_updating_weather_data) + " (" + index + "/" + total + ")")
                .setBadgeIconType(NotificationCompat.BADGE_ICON_NONE)
                .setPriority(NotificationCompat.PRIORITY_MIN)
                .setProgress(0, 0, true)
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setAutoCancel(false)
                .setOngoing(false);
    }

    public abstract int getForegroundNotificationId();

    @Override
    public void onUpdateCompleted(@NonNull Location location, @Nullable Weather old,
                                  boolean succeed, int index, int total) {
        super.onUpdateCompleted(location, old, succeed, index, total);
        if (index + 1 != total) {
            NotificationManagerCompat.from(this).notify(
                    getForegroundNotificationId(),
                    getForegroundNotification(index + 2, total).build()
            );
        }
    }
}
