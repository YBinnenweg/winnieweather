package nodomain.enlimaa.winnieweather.background.polling.permanent.update;

import android.content.Context;

import java.util.List;

import nodomain.enlimaa.winnieweather.WinnieWeather;
import nodomain.enlimaa.winnieweather.background.polling.basic.ForegroundUpdateService;
import nodomain.enlimaa.winnieweather.basic.model.location.Location;
import nodomain.enlimaa.winnieweather.background.polling.permanent.PermanentServiceHelper;
import nodomain.enlimaa.winnieweather.remoteviews.NotificationUtils;
import nodomain.enlimaa.winnieweather.remoteviews.WidgetUtils;

/**
 * Foreground normal update service.
 * */
public class ForegroundNormalUpdateService extends ForegroundUpdateService {

    @Override
    public void updateView(Context context, Location location) {
        WidgetUtils.updateWidgetIfNecessary(context, location);
        NotificationUtils.updateNotificationIfNecessary(context, location);
    }

    @Override
    public void updateView(Context context, List<Location> locationList) {
        WidgetUtils.updateWidgetIfNecessary(context, locationList);
    }

    @Override
    public void handlePollingResult(boolean failed) {
        PermanentServiceHelper.updatePollingService(this, failed);
    }

    @Override
    public int getForegroundNotificationId() {
        return WinnieWeather.NOTIFICATION_ID_UPDATING_NORMALLY;
    }
}
