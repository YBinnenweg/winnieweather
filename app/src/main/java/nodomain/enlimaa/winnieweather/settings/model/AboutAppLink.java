package nodomain.enlimaa.winnieweather.settings.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import nodomain.enlimaa.winnieweather.R;

/**
 * About app link.
 * */

public class AboutAppLink {

    public int iconRes;
    public String title;
    public String url;
    public boolean email;

    private AboutAppLink(int iconRes, String title, String url, boolean email) {
        this.iconRes = iconRes;
        this.title = title;
        this.url = url;
        this.email = email;
    }

    public static List<AboutAppLink> buildLinkList(Context context) {
        List<AboutAppLink> list = new ArrayList<>(2);

        list.add(new AboutAppLink(
                R.drawable.ic_github,
                context.getString(R.string.gitHub),
                "https://gitlab.com/YBinnenweg/winnieweather",
                false
        ));
        list.add(new AboutAppLink(
                R.drawable.ic_email,
                context.getString(R.string.email),
                "mailto:nobody@example.com",
                true
        ));

        return list;
    }
}
