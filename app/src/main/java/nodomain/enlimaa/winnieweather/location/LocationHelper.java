package nodomain.enlimaa.winnieweather.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.util.List;
import java.util.TimeZone;

import nodomain.enlimaa.winnieweather.basic.model.location.Location;
import nodomain.enlimaa.winnieweather.basic.model.option.provider.WeatherSource;
import nodomain.enlimaa.winnieweather.db.DatabaseHelper;
import nodomain.enlimaa.winnieweather.location.service.AndroidLocationService;
import nodomain.enlimaa.winnieweather.location.service.LocationService;
import nodomain.enlimaa.winnieweather.settings.SettingsOptionManager;
import nodomain.enlimaa.winnieweather.utils.NetworkUtils;
import nodomain.enlimaa.winnieweather.weather.service.AccuWeatherService;
import nodomain.enlimaa.winnieweather.weather.service.WeatherService;

/**
 * Location helper.
 * */

public class LocationHelper {

    @NonNull private final LocationService locationService;
    @NonNull private final WeatherService accuWeather;

    public LocationHelper(Context context) {
        switch (SettingsOptionManager.getInstance(context).getLocationProvider()) {
            default: // NATIVE
                locationService = new AndroidLocationService(context);
                break;
        }

        accuWeather = new AccuWeatherService();
    }

    public void requestLocation(Context context, Location location, boolean background,
                                @NonNull OnRequestLocationListener l) {
        if (!NetworkUtils.isAvailable(context)
                || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            l.requestLocationFailed(location);
            return;
        }
        if (background
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            l.requestLocationFailed(location);
            return;
        }

        // 1. get location by location service.
        // 2. get available location by weather service.

        locationService.requestLocation(
                context,
                result -> {
                    if (result == null) {
                        l.requestLocationFailed(location);
                        return;
                    }

                    location.updateLocationResult(
                            result.latitude, result.longitude, TimeZone.getDefault(),
                            result.country, result.province, result.city, result.district,
                            result.inChina
                    );

                    requestAvailableWeatherLocation(context, location, l);
                }
        );
    }

    private void requestAvailableWeatherLocation(Context context,
                                                 @NonNull Location location,
                                                 @NonNull OnRequestLocationListener l) {
        switch (SettingsOptionManager.getInstance(context).getWeatherSource()) {
            case ACCU:
                location.setWeatherSource(WeatherSource.ACCU);
                accuWeather.requestLocation(context, location, new AccuLocationCallback(context, location, l));
                break;
        }
    }

    public void cancel() {
        locationService.cancel();
        accuWeather.cancel();
    }

    public String[] getPermissions() {
        String[] permissions = locationService.getPermissions();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            return permissions;
        }

        String[] qPermissions = new String[permissions.length + 1];
        System.arraycopy(permissions, 0, qPermissions, 0, permissions.length);
        qPermissions[qPermissions.length - 1] = Manifest.permission.ACCESS_BACKGROUND_LOCATION;

        return qPermissions;
    }

    // interface.

    public interface OnRequestLocationListener {
        void requestLocationSuccess(Location requestLocation);
        void requestLocationFailed(Location requestLocation);
    }
}

class AccuLocationCallback implements WeatherService.RequestLocationCallback {

    private Context context;
    private Location location;
    private LocationHelper.OnRequestLocationListener listener;

    AccuLocationCallback(Context context, Location location,
                         @NonNull LocationHelper.OnRequestLocationListener l) {
        this.context = context;
        this.location = location;
        this.listener = l;
    }

    @Override
    public void requestLocationSuccess(String query, List<Location> locationList) {
        if (locationList.size() > 0) {
            Location location = locationList.get(0).setCurrentPosition();
            DatabaseHelper.getInstance(context).writeLocation(location);
            listener.requestLocationSuccess(location);
        } else {
            requestLocationFailed(query);
        }
    }

    @Override
    public void requestLocationFailed(String query) {
        listener.requestLocationFailed(location);
    }
}
