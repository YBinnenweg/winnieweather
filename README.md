# WinnieWeather

## What is this

[WinnieWeather](https://en.wikipedia.org/wiki/Winnie-the-Pooh#Censorship_in_China) is a fork of [GeometricWeather](https://github.com/WangDaYeeeeee/GeometricWeather) without China-specific features. Amongst these are China-specific location providers, weather providers etc.

The China-specific location providers in GeometricWeather seem to be implemented via closed source libraries provided by said location providers (Baidu is amongst them). I am personally not comfortable with these, hence why I forked GeometricWeather.

## Donations

If you like this application you should donate to [the original developer](https://github.com/WangDaYeeeeee/GeometricWeather). If you don't want to do that, [donate to Wikipedia](https://donate.wikimedia.org) instead.